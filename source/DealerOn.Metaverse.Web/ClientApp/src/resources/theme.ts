import { Color } from "excalibur"

export const Light = Color.fromHex("#EBEBEB");
export const Gray = Color.fromHex("#C2C2C2");
export const Sky = Color.fromHex("#0096CC");
export const Orange = Color.fromHex("#FF680A");
export const Blue = Color.fromHex("#274E91");